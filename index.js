const express = require('express');

//express function is our server stored in a constant variable app
const app = express();

const PORT = 3001;


//const User = require('./models/User'); removing this by commenting because another folder is already created for routes

//Middlewares
//Express.json is an express framework to parse incoming json payloads
app.use(express.json())
app.use (express.urlencoded ({extended:true}));

//Connecting userRoutes module to index.js entry point
const userRoutes = require('./routes/userRoutes');

//require mongoose module to be used in our entry point file index.js
const mongoose = require('mongoose');

//connect to mongoDB database
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.fwkne.mongodb.net/s31?retryWrites=true&w=majority',{useNewUrlParser: true, useUnifiedTopology: true}
	);

//notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => 
  console.log('Connected to Database')
);


//routes
	//http://localhost:3001/users



	//Request
		//params
		//body
		//url
		//HTTP methods
			//GET
			//POST
			//PUT
			//PATCH
			//DELETE
		//headers
			//Authorization

	//Response
			//Send

/* Removing routes in index.js by commenting
app.get('/users', (req, res) => {
	//console.log("hello")
	res.send('Hello')
})


app.post('/users', (req, res) => {
	//console.log(req.body)  this line contains object
	//console.log(req)
	let name = req.body.name

	res.send(`Hello ${name}`)
})
*/

//middleware entry point url(root url before any endpoints)
app.use("/api/users", userRoutes);

//Server listening to port 3001
app.listen(PORT, ()=> console.log(`Server is running at ${PORT}`));
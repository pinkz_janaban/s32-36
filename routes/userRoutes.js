
const express = require('express');
//Create routes

//Router() handles the requests
const router = express.Router();


//Syntax: router.HTTPmethod('urI', <request listener>)
router.get('/', (req, res) => {
	//console.log("Hello from userRoutes")

	res.send("Hello from userRoutes")
})


module.exports = router;